using NUnit.Framework;

namespace Supermarket.Tests.Nunit
{
    [TestFixture]
    public class RuleListTests
    {
        [Test]
        public void TestGetRulesSortedByCountDescWorksCorrectly()
        {
            var ruleList = new RuleList();
            ruleList.AddRule(new Rule(3, 35));
            ruleList.AddRule(new Rule(8, 87));
            ruleList.AddRule(new Rule(2, 23));
            ruleList.AddRule(new Rule(9, 98));
            ruleList.AddRule(new Rule(1, 12));
            Assert.AreEqual(98, ruleList.GetRulesSortedByCountDesc()[0].Price);
            Assert.AreEqual(87, ruleList.GetRulesSortedByCountDesc()[1].Price);
            Assert.AreEqual(35, ruleList.GetRulesSortedByCountDesc()[2].Price);
            Assert.AreEqual(23, ruleList.GetRulesSortedByCountDesc()[3].Price);
            Assert.AreEqual(12, ruleList.GetRulesSortedByCountDesc()[4].Price);
        }

        [Test]
        public void TestAddRuleThrowsErrorWhenDuplicateCountUsed()
        {
            var ruleList = new RuleList();
            ruleList.AddRule(new Rule(2, 23)); 
            Assert.Throws<System.ArgumentException>(() => ruleList.AddRule(new Rule(2, 57)));
        }
    }
}