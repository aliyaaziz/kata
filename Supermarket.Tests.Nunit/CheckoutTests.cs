using System;
using NUnit.Framework;
using Supermarket.Interfaces;

namespace Supermarket.Tests.Nunit
{
    [TestFixture]
    public class CheckoutTests
    {
        PricingRules _pricingRules;
        IPricingRules _doublePricingRules;
        public CheckoutTests()
        {
            _pricingRules = new PricingRules();
            _pricingRules.Add("A", 1, 50);
            _pricingRules.Add("A", 3, 130);
            _pricingRules.Add("B", 1, 30);
            _pricingRules.Add("B", 2, 45);
            _pricingRules.Add("C", 1, 20);
            _pricingRules.Add("D", 1, 15);

            _doublePricingRules = new DoublePricingRules();
            _doublePricingRules.Add("A", 1, 50);
            _doublePricingRules.Add("A", 3, 130);
            _doublePricingRules.Add("B", 1, 30);
            _doublePricingRules.Add("B", 2, 45);
            _doublePricingRules.Add("C", 1, 20);
            _doublePricingRules.Add("D", 1, 15);
        }

        [Test]
        public void TestCheckoutCostCorrectWithoutMultibuy()
        {
            var checkout = new Checkout(_pricingRules);           
            checkout.Scan("A"); 
            checkout.Scan("B"); 
            checkout.Scan("C"); 
            checkout.Scan("D"); 
            Assert.AreEqual(115, checkout.GetTotalPrice());
        }

        [Test]
        public void TestMultbuyPriceCorrectWhenScannedInMixedOrder()
        {
            var checkout = new Checkout(_pricingRules);
            checkout.Scan("A"); 
            checkout.Scan("B"); 
            checkout.Scan("A"); 
            checkout.Scan("A"); 
            checkout.Scan("B"); 
            Assert.AreEqual(175, checkout.GetTotalPrice());
        }

        [Test]
        public void TestMultbuyPriceCorrectWhenScannedInMixedOrderDoubled()
        {
            var checkout = new Checkout(_doublePricingRules);
            checkout.Scan("A"); 
            checkout.Scan("B"); 
            checkout.Scan("A"); 
            checkout.Scan("A"); 
            checkout.Scan("B"); 
            Assert.AreEqual(350, checkout.GetTotalPrice());
        }

        [Test]
        public void TestMultpleSpecialBuysReturnCorrectPrice()
        {
            var checkout = new Checkout(GetTestPricingRulesWithMultipleSpecialBuys());
            checkout.Scan("A"); checkout.Scan("A"); 
            checkout.Scan("A"); checkout.Scan("A"); 
            checkout.Scan("A"); checkout.Scan("A"); 
            checkout.Scan("A"); checkout.Scan("A"); 
            checkout.Scan("A"); checkout.Scan("A"); 
            checkout.Scan("A"); checkout.Scan("A");
            checkout.Scan("A"); checkout.Scan("A");
            checkout.Scan("A");  //15
            Assert.AreEqual(530, checkout.GetTotalPrice());
        }

        private PricingRules GetTestPricingRulesWithMultipleSpecialBuys()
        {
            var _pricingRulesComplex  = new PricingRules();
            _pricingRulesComplex.Add("A", 1, 50);
            _pricingRulesComplex.Add("A", 3, 130);
            _pricingRulesComplex.Add("A", 10, 300);
            return _pricingRulesComplex;
        }

        [Test]
        public void TestErrorThrownIfSingleUnitPriceMissing()
        {
            var checkout = new Checkout(GetTestPricingRulesWithMissingSinglePrice());
            checkout.Scan("A"); 
            checkout.Scan("A");
            Assert.Throws<Exception>(() => checkout.GetTotalPrice());
        }

        private PricingRules GetTestPricingRulesWithMissingSinglePrice()
        {
            var _pricingRulesComplex  = new PricingRules();
            _pricingRulesComplex.Add("A", 3, 130);
            _pricingRulesComplex.Add("A", 10, 300);
            return _pricingRulesComplex;
        }
    }
}