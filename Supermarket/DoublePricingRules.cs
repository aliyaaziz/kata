using System;
using System.Collections.Generic;
using Supermarket.Interfaces;

namespace Supermarket
{
    public class DoublePricingRules : IPricingRules
    {
        IPricingRules _pricingRules;
        public DoublePricingRules()
        {
            _pricingRules = new PricingRules();
        }
        
        public void Add(string item, int count, int price)
        {
            _pricingRules.Add(item, count, price*2);
        }

        public int GetPrice(string item, int scanned)
        {
            return _pricingRules.GetPrice(item, scanned);
        }
    }
}