namespace Supermarket
{
    public class Rule
    {
        public int Count{ get; }
        public int Price{ get; }

        public Rule(int count, int price)
        {
            this.Count = count;
            this.Price = price;
        }
    }
}