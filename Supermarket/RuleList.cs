using System.Collections.Generic;
using Supermarket.Interfaces;

namespace Supermarket
{
    public class RuleList: IRuleList
    {
        private SortedDictionary<int, int> _rules;
        private RulesComparer _rulesComparer;

        public RuleList()
        {
            _rulesComparer = new RulesComparer();
            _rules = new SortedDictionary<int, int>(_rulesComparer);
        }

        public RuleList(Rule rule): this()
        {
            AddRule(rule);
        }

        public void AddRule(Rule rule)
        {
            _rules.Add(rule.Count, rule.Price);
        }

        public IList<Rule> GetRulesSortedByCountDesc()
        {
            var rules = new List<Rule>();
            foreach(var rule in _rules)
            {
                rules.Add(new Rule(rule.Key, rule.Value));
            }
            return rules;
        }

        private class RulesComparer : IComparer<int>
        {
            public int Compare(int x, int y)
            {
                if(x == y) return 0;
                else if(x < y) return 1;
                else return -1;
            }
        }
    }
}