using System;
using System.Collections.Generic;
using Supermarket.Interfaces;

namespace Supermarket
{
    public class PricingRules : IPricingRules
    {
        IDictionary<string, IRuleList> _rules;
        public PricingRules()
        {
            _rules = new Dictionary<string, IRuleList>();
        }
        public void Add(string item, int count, int price)
        {
            var newRule = new Rule(count, price);
            if(_rules.ContainsKey(item))
            {
                var itemRules = _rules[item];
                itemRules.AddRule(newRule);
            }
            else{
                _rules.Add(item, new RuleList(newRule));
            }
        }

        public int GetPrice(string item, int scanned)
        {
            int currentPrice = 0;
            int itemsLeftToPrice = scanned;

            if(_rules.ContainsKey(item))
            {
                foreach(var rule in _rules[item].GetRulesSortedByCountDesc())
                {
                    var timesToApply = itemsLeftToPrice / rule.Count;
                    currentPrice += timesToApply * rule.Price;
                    itemsLeftToPrice -= timesToApply * rule.Count;
                }
            }
            if(itemsLeftToPrice > 0)
            {
                throw new Exception($"No price rule given for single unit of {item}");
            }
            return currentPrice;
        }
    }
}