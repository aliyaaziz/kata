using System.Collections.Generic;

namespace Supermarket.Interfaces
{
    public interface IRuleList
    {
        void AddRule(Rule rule);
        IList<Rule> GetRulesSortedByCountDesc();
    }
}