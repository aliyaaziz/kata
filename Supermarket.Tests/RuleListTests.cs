using System;
using Xunit;

namespace Supermarket.Tests
{
    public class RuleListTests
    {
        [Fact]
        public void TestGetRulesSortedByCountDescWorksCorrectly()
        {
            var ruleList = new RuleList();
            ruleList.AddRule(new Rule(3, 35));
            ruleList.AddRule(new Rule(8, 87));
            ruleList.AddRule(new Rule(2, 23));
            ruleList.AddRule(new Rule(9, 98));
            ruleList.AddRule(new Rule(1, 12));
            Assert.Equal(98, ruleList.GetRulesSortedByCountDesc()[0].Price);
            Assert.Equal(87, ruleList.GetRulesSortedByCountDesc()[1].Price);
            Assert.Equal(35, ruleList.GetRulesSortedByCountDesc()[2].Price);
            Assert.Equal(23, ruleList.GetRulesSortedByCountDesc()[3].Price);
            Assert.Equal(12, ruleList.GetRulesSortedByCountDesc()[4].Price);
        }

        [Fact]
        public void TestAddRuleThrowsErrorWhenDuplicateCountUsed()
        {
            var ruleList = new RuleList();
            ruleList.AddRule(new Rule(2, 23)); 
            Assert.Throws<System.ArgumentException>(() => ruleList.AddRule(new Rule(2, 57)));
        }
    }
}